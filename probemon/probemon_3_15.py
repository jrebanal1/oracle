#!/usr/bin/python

import time
import datetime
import argparse
import netaddr
import sys
import logging
import numpy as np
from scapy.all import *
from pprint import pprint
from logging.handlers import RotatingFileHandler
from logging.handlers import TimedRotatingFileHandler

NAME = 'probemon'
DESCRIPTION = "a command line tool for logging 802.11 probe request frames"

DEBUG = False

macs = []
previous_rssi = []
previous_packet = []

def build_packet_callback(time_fmt, logger, delimiter, mac_info, ssid, rssi):

	def packet_callback(packet):

		# setting a threshold for room from center to door (7.5 m)
		if -(256-ord(packet.notdecoded[-4:-3])) < -45.1085-7.188577:
			return
		
		global previous_packet
		global previous_rssi
		global macs

		# check for 802.11 specification Dot11
		if not packet.haslayer(Dot11):
			return
	
		# we are looking for management frames with a probe subtype
		# if neither match we are done here
		if packet.type != 0 or packet.subtype != 0x04:
			return
####################################################################################################
# first time getting packet (previous_packet is empty), do regular stuff
		if previous_packet == []:
			# list of output fields
			fields = []
			# determine preferred time format 
			log_time = str(int(time.time()))
			if time_fmt == "iso":
			#log_time = datetime.datetime.now().isoformat()
				log_time = str(time.strftime("%H:%M"))

			fields.append(log_time)

			# append the mac address itself
			fields.append('$' + packet.addr2)
			#if str(packet.addr2) == "4c:66:41:b4:33:eb":
			#	print("This is me.")
			#	print(str(-(256-ord(packet.notdecoded[-4:-3]))))
			#elif str(packet.addr2) == "94:65:2d:6e:73:b0":
			#	print("we found the sandman...")
			#	print(str(-(256-ord(packet.notdecoded[-2:-1]))))


			# parse mac address and look up the organization from the vendor octets
			if mac_info:
				try:
					parsed_mac = netaddr.EUI(packet.addr2)
					fields.append('$' + parsed_mac.oui.registration().org + '$')
				except netaddr.core.NotRegisteredError, e:
					fields.append('$' + 'UNKNOWN' + '$')

			# include the SSID in the probe frame
			#if ssid:
			#	fields.append('$' + packet.info + '$')
			
			if rssi:
				rssi_val = -(256-ord(packet.notdecoded[-4:-3]))
			#rssi_val = -(256-ord(packet.notdecoded[-2:-1]))
				fields.append('$' + str(rssi_val))
				previous_rssi.append(rssi_val)

			#if str(packet.addr2) not in macs:
			#	macs.append(str(packet.addr2))
			#	fields.append('$$')
			#elif str(packet.addr2) in macs:
			#	fields.append('$Was here before.$')

			previous_packet = fields
		
####################################################################################################
# check that not first packet (something in previous_packet), old device in burst probe? append rssi
		if previous_packet:
			previous_mac = str(previous_packet[1])
			if previous_mac == ('$' + str(packet.addr2)):
				if rssi: 
					rssi_val = -(256-ord(packet.notdecoded[-4:-3]))
					previous_rssi.append(rssi_val)		
####################################################################################################
# if previous packet was not from same device, then log as normally
			else:
				if previous_rssi:
					
					# determining average rssi from same device probes
					#avg_rssi = (sum(previous_rssi) / len(previous_rssi))
					avg_rssi = np.average(previous_rssi)	
					std_dev_rssi = np.std(previous_rssi)	

					# Removing outlier method 1
					def drop_outliers(x):
						if abs(x - avg_rssi) <= std_dev_rssi:							
							return x
					rm_outlier_rssi = filter(drop_outliers, previous_rssi)	
					new_avg_rssi = int(round(np.average(rm_outlier_rssi)))
		
					# Removing outlier method 2 - Modified Z-score method -> doesnt work yet (warnings)
					#threshold = 3.5					
					#median_y = np.median(previous_rssi)	
					#median_abs_dev_y = np.median([abs(y - median_y) for y in previous_rssi])
					#mod_z_scores = [0.6745*(y - median_y) / median_abs_dev_y for y in previous_rssi]
					#testing_outliers = (np.abs(mod_z_scores) > threshold)
					#new_avg_rssi = int(round(np.average(testing_outliers)))

					previous_packet[3] = (str(new_avg_rssi))
					# reset previous rssi values for new device probes
					previous_rssi = []
					#log the previous packet with average rssi value
					logger.info(delimiter.join(previous_packet))
			
				# list of output fields
				fields = []
				# determine preferred time format 
				log_time = str(int(time.time()))
				if time_fmt == "iso":
					log_time = str(time.strftime("%H:%M"))
					fields.append(log_time)
	
				# append the mac address itself
				fields.append('$' + packet.addr2)
				#if str(packet.addr2) == "4c:66:41:b4:33:eb":
				#	print("This is me.")
				#	print(str(-(256-ord(packet.notdecoded[-4:-3]))))
				#elif str(packet.addr2) == "94:65:2d:6e:73:b0":
				#	print("This is Sudman")
				#	print(str(-(256-ord(packet.notdecoded[-2:-1]))))
	
				# parse mac address and look up the organization from the vendor octets
				if mac_info:
					try:
						parsed_mac = netaddr.EUI(packet.addr2)
						fields.append('$' + parsed_mac.oui.registration().org + '$')
					except netaddr.core.NotRegisteredError, e:
						fields.append('$' + 'UNKNOWN' + '$')
	
				# include the SSID in the probe frame
				#if ssid:
				#	fields.append('$' + packet.info + '$')
				
				if rssi:
					rssi_val = -(256-ord(packet.notdecoded[-4:-3]))
				#rssi_val = -(256-ord(packet.notdecoded[-2:-1]))
					fields.append('$' + str(rssi_val))
					previous_rssi.append(rssi_val)
	
				#if str(packet.addr2) not in macs:
				#	macs.append(str(packet.addr2))
				#	fields.append('$$')
				#elif str(packet.addr2) in macs:
				#	fields.append('$Was here before.$')

				previous_packet = fields # previous packet is updated

####################################################################################################
	return packet_callback

def main():
	parser = argparse.ArgumentParser(description=DESCRIPTION)
	parser.add_argument('-i', '--interface', help="capture interface")
	parser.add_argument('-t', '--time', default='iso', help="output time format (unix, iso)")
	parser.add_argument('-o', '--output', default='probemon.log', help="logging output location")
	parser.add_argument('-b', '--max-bytes', default=5000000, help="maximum log size in bytes before rotating")
	parser.add_argument('-c', '--max-backups', default=99999, help="maximum number of log files to keep")
	parser.add_argument('-d', '--delimiter', default='', help="output field delimiter")
	parser.add_argument('-f', '--mac-info', action='store_true', help="include MAC address manufacturer")
	parser.add_argument('-s', '--ssid', action='store_true', help="include probe SSID in output")
	parser.add_argument('-r', '--rssi', action='store_true', help="include rssi in output")
	parser.add_argument('-D', '--debug', action='store_true', help="enable debug output")
	parser.add_argument('-l', '--log', action='store_true', help="enable scrolling live view of the logfile")
	args = parser.parse_args()

	if not args.interface:
		print "error: capture interface not given, try --help"
		sys.exit(-1)
	
	DEBUG = args.debug

	# setup our rotating logger
	logger = logging.getLogger(NAME)
	logger.setLevel(logging.INFO)

	# changing maxBytes will keep logging until file is up to maxBytes and start new file 
	# if name of text is "myFile.txt", after it reaches maxBytes, it will keep logging on new file called "myFile1.txt"
	# backupCount determines how many files it will keep creating before rewriting first file (goes up to "myFile2.txt")
	#handler = RotatingFileHandler(args.output, maxBytes=2000000, backupCount=1)
	#handler = RotatingFileHandler(args.output, maxBytes=args.max_bytes, backupCount=args.max_backups)

	# up to two additional files over original, updates oldest file every 1 minute
 	#handler = TimedRotatingFileHandler(args.output, when="m", interval=1, backupCount=0)
	handler = logging.FileHandler(args.output)	
	logger.addHandler(handler)

	if args.log:
		logger.addHandler(logging.StreamHandler(sys.stdout))
	
	# find packet information
	built_packet_cb = build_packet_callback(args.time, logger, args.delimiter, args.mac_info, args.ssid, args.rssi)
	sniff(iface=args.interface, prn=built_packet_cb, store=0)

if __name__ == '__main__':
	#s = float(time.strftime('%S'))
	#delayed = 60.0 - s
	main()
	#time.sleep(delayed)

	#while True:
	#	time.sleep(60)
	#	main()
