#!/bin/bash

# Run as root.
# Send probemon.log file as .txt file to my laptop regularly.

echo "Starting automated Bluetooth file transfer routine."

while true; do

cp probemon.log probemon.txt # change to .txt for easier processing
sudo obexftp -b 18:CF:5E:EA:B5:82 -c / -p probemon.txt # send to laptop
echo "Upload complete"
sudo truncate -s 0 probemon.log # clear log after send
sleep 20 # wait

done

