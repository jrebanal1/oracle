import matplotlib
matplotlib.use('TKAgg')
import string
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
from matplotlib import style

fig = plt.figure()
ax1 = fig.add_subplot(1,1,1)
source = "/home/daru/Downloads"
def animate(i):
    f = source + "/17/" + "17.txt" 
    pullData = open(f,"r").read()
    #pullData = open("samp.txt","r").read()
    dataArray = pullData.split('\n')

    xar = []
    yar = []
    for eachLine in dataArray:
        if len(eachLine)>1:
            x,y = eachLine.split(':')
            xar.append(str(x))
            yar.append(int(y))
	    ind = np.arange(len(xar))
    ax1.clear()
    ax1.bar(ind, yar, align='center')
    plt.xticks(ind, xar)
ani = animation.FuncAnimation(fig, animate, interval=1000)
plt.show()
